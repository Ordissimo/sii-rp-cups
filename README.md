Seiko Instruments Inc.
Linux CUPS Filter for SII thermal printers



[[[ Overview ]]]
This document describes the specifications of filter on CUPS
for Seiko Instruments Inc.(SII) printer.
Under the GNU General Public License or GPL,this software set is freely licensed to you.



[[[ Supported printers ]]]
The targeted printers  are as follows:
* RP-E10
* RP-E11
* RP-D10



[[[ Supported interface ]]]
* USB
* Serial
* TCP/IP


[[[ Software Requirements ]]]
The following software or etc might be necessary 
 to generate raster data from PS or image data in CUPS.
* ghostscript
* libjpeg
* libpng
* libtiff



[[[ Install ]]]
*** Manual install ***
	For the distribution that doesn't support the RPM package manager.
	The manual install is at least necessary to install the following software
	 that are needed to compile and install this software beforehand.
	* C compiler
	* CUPS & CUPS Development Environment


    ((Installation example))
	Extract the filter source.
	> tar zxvf sii_rp_cups-1.1.0.tar.gz

	> cd sii_rp_cups-1.1.0/

	Execute "./configure".
	> ./configure

	Execute "make".
	> make

	> su

	# (enter your superuser password)

	Execute "make install" by superuser.
	# make install

	Add printer from web browser.
	Access http://localhost:631 and add a new SII printer.

    ((Note))
	* If two or more CUPS is installed or CUPS is installed in original folder,
	   it is necessary to specify the folder CUPS installed by 
	   [ --with-cups-dir=<CUPS installed directory> ] parameter.
		*** example ***
		> ./configure --with-cups-dir=/usr/local/

	* CUPS is restarted automatically.
	   mime.convs is changed (overwritten) by "make install".

[[[ Add Printer ]]]
　　　The printer is added on the management screen of a web browser. 

	1) Access http://localhost:631 and click on the "Administration" TAB.

	2) Click on the "Add Printer" button.

	3) Select the port where the printer is connected.
	   Click on the "Continue" button.
	   
	4) In case of serial connection, you are prompted to enter the following four things.
	   The setting example is shown below. 

	        *Band Rate	: 115200
	        *Parity	: None
	        *Data Bits	: 8
		*Flow Control	: RTS/CTS (Hardware)

	   Click on the "Continue" button.
	   
	5) You are prompted to enter the following three things:

	      *Name:
	      *Description:
   	      *Location:
 
	   Only the name field is required.

	   Click on the "Continue" button.

	6) You are prompted to select "Model" from "Manufacturer" or 
	   to enter "Provide a PPD File".
	   
	   Click on the "Browse..." button.
	   And select the PPD file necessary for your printer from the
	    following folder.

	     /usr/share/cups/model/

	   Click on the "Add Printer" button.

	7) Select default options, then click on "Set Default Options " button. 
	   Then, the installation is completed. 

	*** Note ***
	If prompted for a username and password, enter root as the username
	 and then the root password. 

	When access for /dev/ttyS0 is not permitted by installation setting
	 of CUPS, it is impossible to print.
	In that case, it is necessary to change permission as follows.
	# chmod a+rw /dev/ttyS0
	   
	   
[[[ configuration items ]]]

*** Print Quality ***
Print Quality [203 dpi (Normal mode) or 101dpi (Draft mode)] can be selected.

*** Media Size ***
Print paper size can be specified.
XX mm * YY mm					: Paper size of XX mm in width and YY mm in length
A4 reduced to XX mm				: A4-sized paper reduced to the paper size with a width of XX mm
Letter reduced to XX mm			: Letter-sized paper reduced to the paper size with a width of XX mm
Custom Paper Size (XX mm * YY mm)		: Custom Paper Size of XX mm in width and YY mm in length
* Selectable paper sizes vary depending on the models.
* You can change the length of the Custom Paper Size by using the Custom paper setting program.
* Refer to Custom paper setting program for details.

*** Cut Mode ***
Cut Mode (Full Cut, Partial Cut, or No Cut) can be selected.

*** Print Density ***
Print Density (70%/80%/90%/100%/110%/120%/130%) can be selected.

*** Cut Timing ***
Cut Timing (Each Page, or Each Document) can be selected.
*Each Page 		: The printer will be cut by page.
*Each Document 	: The printer will cut the paper at the end of the document.

*** Blank Image ***
Blank Image (Feed, or Non feed) can be selected.
*Feed 			: The printer will print on a page with a fixed length.
*Non feed 		: The printer will not feed paper for the top and bottom page margins.

*** Dither Type ***
Type of dithering (None, Screen, or Error Diffusion) can be selected.


[[[ Custom paper setting program ]]]

    ((Custom paper setting example))

	Execute Custom paper setting program by super user.
	> su
	# (enter your superuser password)
	# sii_set_custompage [CUPS PrinterName] [Height]
	    [CUPS PrinterName] : Printer name set with CUPS
	    [Height]           : Paper Length (Unit=mm)


[[[ Other Notes ]]]
* The print with CUPS cannot be used with the communication library. 
* For TCP/IP connection, set Network Printer Receive Timeout(s) to 300 in the TCP/IP
   port on the WEB settings screen.
* For TCP/IP connection, please use MS5-3 of Function Setting with Enable. 
* Please use MS40-8 of Function Setting with RxD. 
* Please refer to "RP-E10 SERIES THERMAL PRINTER TECHNICAL REFERENCE" or 
   "RP-D10 SERIES THERMAL PRINTER TECHNICAL REFERENCE"of printer about the function switch.
* For OpenSUSE 12.1, Please use this product after confirming following information. 
   http://lists.opensuse.org/opensuse-bugs/2012-04/msg01203.html

[[[ Versions ]]]
Ver 1.0.0 (Jul. 2012)
	* First public release
Ver 1.1.0 (Aug. 2013)
	* Add new printer models (RP-D10 Serise)


SII is not liable for any damages, losses, caused by or relating to the use of this product 
nor for any expenses incurred for defraying such, and we cannot do all the supports for this software.
The content of this software might modify without the permission.

Copyright(c) 2013 by Seiko Instruments Inc. All rights reserved.
